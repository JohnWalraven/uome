package ravensoftware.uome;

import android.content.Intent;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class AddItem extends AppCompatActivity {

    private TextView description = null;
    private TextView quantity = null;
    private TextView unitCost = null;
    private TextView total = null;
    private Button saveItemInfo = null;
    private CheckBox taxable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        final Intent saveNewItemIntent = new Intent(this, NewInvoice.class);
        final Bundle extras = new Bundle();

        description = (TextView) findViewById(R.id.itemDescription);
        quantity = (TextView) findViewById(R.id.quantity);
        unitCost = (TextView) findViewById(R.id.unitCost);
        taxable = (CheckBox) findViewById(R.id.taxable);

        total = (TextView) findViewById(R.id.total);
        saveItemInfo = (Button) findViewById(R.id.saveItemInfo);

        saveItemInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfNull();
                total.setText(calculateCost(quantity, unitCost));
                if (taxable.isEnabled()) {
                    taxable.setText("Yes");
                } else
                    taxable.setText("No");
                extras.putString("itemDescription", description.getText().toString());
                extras.putString("quantity", quantity.getText().toString());
                extras.putString("unitCost", unitCost.getText().toString());
                extras.putString("taxable", taxable.getText().toString());
                extras.putString("total", total.getText().toString());
                saveNewItemIntent.putExtras(extras);
                setResult(3, saveNewItemIntent);
                finish();
            }
        });
    }

    private String calculateCost(TextView amount, TextView cost) {
        float amountFloat = Float.parseFloat(amount.getText().toString());
        float costFloat = Float.parseFloat(cost.getText().toString());
        float totalAmount = costFloat * amountFloat;
        String strAmount = String.valueOf(totalAmount);
        return strAmount;
    }

    private void checkIfNull() {
        if (description.getText().toString().equals("") == true) {
            description.setText("null");
        }
        if (quantity.getText().toString().equals("") == true) {
            quantity.setText("null");
        }
        if (unitCost.getText().toString().equals("") == true) {
            unitCost.setText("null");
        }
    }

}
