package ravensoftware.uome;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ClientDetails extends AppCompatActivity {

    private static final int PICK_CONTACT = 1;
    private Button saveClientDetails = null;
    private Button importContact = null;
    private TextView clientDetailsName = null;
    private TextView clientDetailsEmail = null;
    private TextView clientDetailsMobile = null;
    private TextView clientDetailsPhone = null;
    private TextView clientDetailsAddressLine1 = null;
    private TextView clientDetailsAddressLine2 = null;
    private TextView clientDetailsAddressLine3 = null;
    private TextView clientDetailsCity = null;
    private TextView clientDetailsAddressPostalCode = null;
    private Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Intent intentBackToInvoice = new Intent(this, NewInvoice.class);
        final Bundle extras = new Bundle();
        intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_details);

        importContact = (Button) findViewById(R.id.importContact);
        clientDetailsName = (TextView) findViewById(R.id.clientDetailsName);
        clientDetailsEmail = (TextView) findViewById(R.id.clientDetailsEmail);
        clientDetailsMobile = (TextView) findViewById(R.id.clientDetailsMobile);
        clientDetailsPhone = (TextView) findViewById(R.id.clientDetailsPhone);

        clientDetailsAddressLine1 = (TextView) findViewById(R.id.clientDetailsAddressLine1);
        clientDetailsAddressLine2 = (TextView) findViewById(R.id.clientDetailsAddressLine2);
        clientDetailsAddressLine3 = (TextView) findViewById(R.id.clientDetailsAddressLine3);
        clientDetailsCity = (TextView) findViewById(R.id.clientDetailsAddressCity);
        clientDetailsAddressPostalCode = (TextView) findViewById(R.id.clientDetailsAddressPostalCode);

        saveClientDetails = (Button) findViewById(R.id.editClientSaveDetails);
        importContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
        saveClientDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkIfNull();
                extras.putString("clientDetailsName", clientDetailsName.getText().toString());
                extras.putString("clientDetailsEmail", clientDetailsEmail.getText().toString());
                extras.putString("clientDetailsMobile", clientDetailsMobile.getText().toString());
                extras.putString("clientDetailsPhone", clientDetailsPhone.getText().toString());

                extras.putString("clientDetailsAddressLine1", clientDetailsAddressLine1.getText().toString());
                extras.putString("clientDetailsAddressLine2", clientDetailsAddressLine2.getText().toString());
                extras.putString("clientDetailsAddressLine3", clientDetailsAddressLine3.getText().toString());
                extras.putString("clientDetailsCity", clientDetailsCity.getText().toString());
                extras.putString("clientDetailsAddressPostalCode", clientDetailsAddressPostalCode.getText().toString());

                intentBackToInvoice.putExtras(extras);
                setResult(2, intentBackToInvoice);
                finish();
            }
        });
    }

    private void checkIfNull() {
        if (clientDetailsName.getText().toString().equals("")) {
            clientDetailsName.setText("null");
        }
        if (clientDetailsEmail.getText().toString().equals("")) {
            clientDetailsEmail.setText("null");
        }
        if (clientDetailsMobile.getText().toString().equals("")) {
            clientDetailsMobile.setText("null");
        }
        if (clientDetailsPhone.getText().toString().equals("")) {
            clientDetailsPhone.setText("null");
        }
        if (clientDetailsAddressLine1.getText().toString().equals("")) {
            clientDetailsAddressLine1.setText("null");
        }
        if (clientDetailsAddressLine3.getText().toString().equals("")) {
            clientDetailsAddressLine3.setText("null");
        }
        if (clientDetailsAddressLine3.getText().toString().equals("")) {
            clientDetailsAddressLine3.setText("null");
        }
        if (clientDetailsCity.getText().toString().equals("")) {
            clientDetailsCity.setText("null");
        }
        if (clientDetailsAddressPostalCode.getText().toString().equals("")) {
            clientDetailsAddressPostalCode.setText("null");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri result = data.getData();


                }
        }
    }
}
