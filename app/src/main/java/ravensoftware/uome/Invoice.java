package ravensoftware.uome;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JWalraven on 19/05/2016.
 */
public class Invoice implements Serializable {

    private String invoiceNumber = null;
    private String businessName = null;
    private String date = null;

    private String clientName = null;
    private String clientEmail = null;
    private String clientMobile = null;
    private String clientPhone = null;

    private String addressLine1 = null;
    private String addressLine2 = null;
    private String addressLine3 = null;
    private String addressCity = null;
    private String addressPostalCode = null;

    private String itemDescription = null;
    private String itemQuantity = null;
    private String itemCost = null;
    private String taxable = null;

    private String totalBeforeTax = null;
    private String taxRate = null;
    private String totalTax = null;
    private String balanceDue = null;

    public Invoice() {

    }

    public void setInvoiceNameDetails(String invoiceNumber, String businessName, String date) {
        this.invoiceNumber = invoiceNumber;
        this.businessName = businessName;
        this.date = date;
    }

    public ArrayList<String> getInvoiceNameDetails() {
        ArrayList<String> invoiceDetails = new ArrayList<>();

        invoiceDetails.add(invoiceNumber);
        invoiceDetails.add(businessName);
        invoiceDetails.add(date);

        return invoiceDetails;
    }

    public void setInvoiceClientDetails(String clientName, String clientEmail, String clientMobile, String clientPhone) {
        this.clientName = clientName;
        this.clientEmail = clientEmail;
        this.clientMobile = clientMobile;
        this.clientPhone = clientPhone;
    }

    public ArrayList<String> getInvoiceClientDetails() {
        ArrayList<String> clientDetails = new ArrayList<>();

        clientDetails.add(clientName);
        clientDetails.add(clientEmail);
        clientDetails.add(clientMobile);
        clientDetails.add(clientPhone);

        return clientDetails;
    }

    public void setClientAddressDetails(String addressLine1, String addressLine2, String addressLine3, String addressCity, String addressPostalCode) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.addressCity = addressCity;
        this.addressPostalCode = addressPostalCode;
    }

    public ArrayList<String> getClientAddressDetails() {
        ArrayList<String> clientAddressDetails = new ArrayList<>();

        clientAddressDetails.add(addressLine1);
        clientAddressDetails.add(addressLine2);
        clientAddressDetails.add(addressLine3);
        clientAddressDetails.add(addressCity);
        clientAddressDetails.add(addressPostalCode);

        return clientAddressDetails;
    }

    public void setItemDetails(String itemDescription, String itemQuantity, String itemCost, String taxable) {
        this.itemDescription = itemDescription;
        this.itemQuantity = itemQuantity;
        this.itemCost = itemCost;
        this.taxable = taxable;
    }

    public ArrayList<String> getItemDetails() {
        ArrayList<String> itemDetails = new ArrayList<>();

        itemDetails.add(itemDescription);
        itemDetails.add(itemQuantity);
        itemDetails.add(itemCost);
        itemDetails.add(taxable);

        return itemDetails;
    }

    public void setBalanceAndTotals(String totalBeforeTax, String taxRate, String balanceDue, String totalTax) {
        this.totalBeforeTax = totalBeforeTax;
        this.taxRate = taxRate;
        this.balanceDue = balanceDue;
        this.totalTax = totalTax;
    }

    public ArrayList<String> getBalanceAndTotals() {
        ArrayList<String> balanceAndTotals = new ArrayList<>();

        balanceAndTotals.add(totalBeforeTax);
        balanceAndTotals.add(taxRate);
        balanceAndTotals.add(totalTax);
        balanceAndTotals.add(balanceDue);

        return balanceAndTotals;
    }

}
