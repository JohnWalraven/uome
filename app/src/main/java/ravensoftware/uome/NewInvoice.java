package ravensoftware.uome;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;

import static ravensoftware.uome.R.layout.activity_new_invoice;

public class NewInvoice extends AppCompatActivity {

    private static final int GET_INVOICE_DETAILS_REQUEST = 1;
    private static final int GET_CLIENT_DETAILS_REQUEST = 2;
    private static final int GET_ITEM_DETAILS_REQUEST = 3;
    private TextView invoiceNumber = null;
    private TextView client = null;
    private TextView addItem = null;
    private TextView date = null;
    private TextView businessNameTXT = null;
    private TextView addItemTotal = null;
    private TextView addItemCompleteTotal = null;
    private TextView totalBeforeTax = null;
    private TextView taxRate = null;
    private TextView itemTotal = null;
    private TextView balanceDue = null;
    private Button saveInvoiceButton = null;
    private Invoice invoice = null;
    private XMLFileManager xmlFileManager = null;
    private Bundle extras = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(activity_new_invoice);

        ActivityCompat.requestPermissions(NewInvoice.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.READ_CONTACTS
                },
                100);

        final Intent intentEditInvoice = new Intent(this, editInvoiceDetails.class);
        final Intent intentClientDetails = new Intent(this, ClientDetails.class);
        final Intent intentAddItem = new Intent(this, AddItem.class);
        final Intent intentPdfViewer = new Intent(this, PdfTabbedActivity.class);

        invoice = new Invoice();
        xmlFileManager = new XMLFileManager(this, invoice);

        String currentDateFormat = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());
        date = (TextView) findViewById(R.id.currentDate);
        try {
            date.setText(currentDateFormat);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        client = (TextView) findViewById(R.id.toClient);
        invoiceNumber = (TextView) findViewById(R.id.invoiceNumber);
        addItem = (TextView) findViewById(R.id.addItemTXT);
        businessNameTXT = (TextView) findViewById(R.id.businessNameTXT);
        addItemTotal = (TextView) findViewById(R.id.addItemTotal);
        addItemCompleteTotal = (TextView) findViewById(R.id.addItemCompleteTotal);
        totalBeforeTax = (TextView) findViewById(R.id.totalBeforeTax);
        saveInvoiceButton = (Button) findViewById(R.id.saveInvoiceButton);
        taxRate = (TextView) findViewById(R.id.taxRate);
        balanceDue = (TextView) findViewById(R.id.balanceDue);
        itemTotal = (TextView) findViewById(R.id.itemTotal);
        taxRate.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    generateTax();
                }
                return false;
            }
        });
        invoiceNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intentEditInvoice, GET_INVOICE_DETAILS_REQUEST);
            }
        });
        client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intentClientDetails, GET_CLIENT_DETAILS_REQUEST);
            }
        });
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(intentAddItem, GET_ITEM_DETAILS_REQUEST);
            }
        });
        saveInvoiceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getBalanceAndTotals();
                try {
                    xmlFileManager.createXMLFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                intentPdfViewer.putExtra("invoice", invoice);
                startActivity(intentPdfViewer);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, requestCode, data);

        if (resultCode == 1) {
            extras = data.getExtras();
            getInvoiceDetails();
            Toast.makeText(NewInvoice.this,
                    "Details received", Toast.LENGTH_LONG).show();
        } else {
            if (resultCode == 2) {
                extras = data.getExtras();
                getClientDetails();
                Toast.makeText(NewInvoice.this,
                        "Details received", Toast.LENGTH_LONG).show();
            } else {
                if (resultCode == 3) {
                    extras = data.getExtras();
                    getItemDetails();
                    Toast.makeText(NewInvoice.this,
                            "Details received", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(NewInvoice.this,
                            "No data received please try again", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    private void getInvoiceDetails() {

        String invoiceName = extras.getString("InvoiceName");
        String businessName = extras.getString("BusinessName");
        String invoiceDate = extras.getString("InvoiceDate");

        invoiceNumber.setText(invoiceName);
        businessNameTXT.setText(businessName);
        date.setText(invoiceDate);

        invoice.setInvoiceNameDetails(invoiceName, businessName, invoiceDate);
    }

    private void getClientDetails() {

        String clientName = extras.getString("clientDetailsName");
        String clientEmail = extras.getString("clientDetailsEmail");
        String clientMobile = extras.getString("clientDetailsMobile");
        String clientPhone = extras.getString("clientDetailsPhone");

        String clientAddressLine1 = extras.getString("clientDetailsAddressLine1");
        String clientAddressLine2 = extras.getString("clientDetailsAddressLine2");
        String clientAddressLine3 = extras.getString("clientDetailsAddressLine3");
        String clientAddressCity = extras.getString("clientDetailsCity");
        String clientAddressPostalCode = extras.getString("clientDetailsAddressPostalCode");

        client.setText(clientName);

        invoice.setInvoiceClientDetails(clientName, clientEmail, clientMobile, clientPhone);
        invoice.setClientAddressDetails(clientAddressLine1, clientAddressLine2, clientAddressLine3, clientAddressCity, clientAddressPostalCode);
    }

    private void getItemDetails() {

        String itemDescription = extras.getString("itemDescription");
        String quantity = extras.getString("quantity");
        String unitCost = extras.getString("unitCost");
        String taxable = extras.getString("taxable");
        String total = extras.getString("total");

        addItemTotal.setText("1 x $" + total);
        addItemCompleteTotal.setText("$" + total);
        totalBeforeTax.setText(total);

        invoice.setItemDetails(itemDescription, quantity, unitCost, taxable);
    }

    private void getBalanceAndTotals() {
        if (taxRate.getText().toString().equals("") == true) {
            taxRate.setText("0");
        }
        String totalBeforeTaxString = totalBeforeTax.getText().toString();
        String taxRateString = taxRate.getText().toString();
        String balanceDueString = balanceDue.getText().toString();
        String totalWithTax = itemTotal.getText().toString();

        invoice.setBalanceAndTotals(totalBeforeTaxString, taxRateString, balanceDueString, totalWithTax);
    }

    private void generateTax() {

        Double amountFloat = Double.parseDouble(totalBeforeTax.getText().toString());
        Double amountTax = 0. + Double.parseDouble("0." + taxRate.getText().toString());
        Double totalAmount = amountFloat * amountTax;
        String strAmount = String.valueOf(totalAmount);
        String totalWithTax = String.valueOf(totalAmount + amountFloat);
        itemTotal.setText("$" + strAmount);
        balanceDue.setText("$" + totalWithTax);
    }
}