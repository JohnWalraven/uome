package ravensoftware.uome;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import static ravensoftware.uome.R.layout.fragment_pdf_layout;


public class PDF_Fragment extends Fragment {
    private TextView date;
    private TextView invoiceNameNumber;
    private TextView invoiceBusinessName;
    private TextView invoiceClientName;
    private TextView invoiceClientAddress1;
    private TextView invoiceClientAddress2;
    private TextView invoiceClientAddress3;
    private TextView invoiceClientCity;
    private TextView invoiceClientPostalCode;
    private TextView invoiceItemDescription;
    private TextView invoiceItemQuantity;
    private TextView invoiceItemRate;
    private TextView invoiceClientAmount;
    private TextView invoiceClientTaxRate;
    private TextView invoiceClientAmountOfTax;
    private TextView invoiceClientAbsoluteTotal;
    private TextView invoiceClientBalanceDue;
    private Invoice invoice;
    private ArrayList<String> invoiceDetails;
    private ArrayList<String> clientDetails;
    private ArrayList<String> clientAddressDetails;
    private ArrayList<String> itemDetails;
    private ArrayList<String> balancesAndTotals;

    public PDF_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(fragment_pdf_layout, container, false);
        Bundle bundle = getActivity().getIntent().getExtras();
        invoice = (Invoice) bundle.getSerializable("invoice");
        invoiceDetails = invoice.getInvoiceNameDetails();
        clientDetails = invoice.getInvoiceClientDetails();
        clientAddressDetails = invoice.getClientAddressDetails();
        itemDetails = invoice.getItemDetails();
        balancesAndTotals = invoice.getBalanceAndTotals();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createInterfaceObjects();
        setInvoiceDetails();
    }

    private void createInterfaceObjects() {
        date = (TextView) getView().findViewById(R.id.setDate);
        invoiceNameNumber = (TextView) getView().findViewById(R.id.invoiceNameNumber);
        invoiceBusinessName = (TextView) getView().findViewById(R.id.invoiceBusinessName);
        invoiceClientName = (TextView) getView().findViewById(R.id.invoiceClientName);
        invoiceClientAddress1 = (TextView) getView().findViewById(R.id.invoiceClientAddress1);
        invoiceClientAddress2 = (TextView) getView().findViewById(R.id.invoiceClientAddress2);
        invoiceClientAddress3 = (TextView) getView().findViewById(R.id.invoiceClientAddress3);
        invoiceClientCity = (TextView) getView().findViewById(R.id.invoiceClientCity);
        invoiceClientPostalCode = (TextView) getView().findViewById(R.id.invoiceClientPostalCode);
        invoiceItemDescription = (TextView) getView().findViewById(R.id.invoiceItemDescripton);
        invoiceItemQuantity = (TextView) getView().findViewById(R.id.invoiceItemQuantity);
        invoiceItemRate = (TextView) getView().findViewById(R.id.invoiceItemRate);
        invoiceClientAmount = (TextView) getView().findViewById(R.id.invoiceClientAmount);
        invoiceClientTaxRate = (TextView) getView().findViewById(R.id.invoiceClientTaxRate);
        invoiceClientAmountOfTax = (TextView) getView().findViewById(R.id.invoiceClientAmountOfTax);
        invoiceClientAbsoluteTotal = (TextView) getView().findViewById(R.id.invoiceClientAbsoluteTotal);
        invoiceClientBalanceDue = (TextView) getView().findViewById(R.id.invoiceClientBalanceDue);
    }

    private void setInvoiceDetails() {
        date.setText(invoiceDetails.get(2));
        invoiceNameNumber.setText(invoiceDetails.get(0));
        invoiceBusinessName.setText(invoiceDetails.get(1));
        invoiceClientName.setText(clientDetails.get(0));
        invoiceClientAddress1.setText(clientAddressDetails.get(0));
        invoiceClientAddress2.setText(clientAddressDetails.get(1));
        invoiceClientAddress3.setText(clientAddressDetails.get(2));
        invoiceClientCity.setText(clientAddressDetails.get(3));
        invoiceClientPostalCode.setText(clientAddressDetails.get(4));
        invoiceItemDescription.setText(itemDetails.get(0));
        invoiceItemQuantity.setText(itemDetails.get(1) + "x");
        invoiceItemRate.setText("$" + itemDetails.get(2));
        invoiceClientAmount.setText("$" + balancesAndTotals.get(0));
        invoiceClientTaxRate.setText("Tax(" + balancesAndTotals.get(1) + "%)");
        invoiceClientAmountOfTax.setText(balancesAndTotals.get(2));
        invoiceClientAbsoluteTotal.setText(balancesAndTotals.get(3));
        invoiceClientBalanceDue.setText(balancesAndTotals.get(3));
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
