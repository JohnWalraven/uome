package ravensoftware.uome;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


import static ravensoftware.uome.R.layout.activity_pdf_tabbed;

public class PdfTabbedActivity extends AppCompatActivity implements fragment_save.OnFragmentInteractionListener, PDF_Fragment.OnFragmentInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Invoice invoice;
    private ArrayList<String> clientDetails;
    private ArrayList<String> invoiceNameDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_tabbed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        invoice = (Invoice) getIntent().getSerializableExtra("invoice");
        clientDetails = invoice.getInvoiceClientDetails();
        invoiceNameDetails = invoice.getInvoiceNameDetails();
        Bundle bundle = new Bundle();
        bundle.putSerializable("invoice", invoice);
        PDF_Fragment pdfFragment = new PDF_Fragment();
        pdfFragment.setArguments(bundle);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_pdf_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // Default code I don't want to remove as It causes many things to go wrong
    }

    public void createPdf() {

        PdfDocument document = new PdfDocument();
        View content = findViewById(R.id.pdf_content);
        int pageNumber = 1;
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(content.getWidth(),
                content.getHeight(), pageNumber).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        content.draw(page.getCanvas());
        document.finishPage(page);

        File directory = new File(Environment.getExternalStorageDirectory() + "/UOME");
        File outputFile = new File(directory + "/" + invoiceNameDetails.get(0) + ".pdf");

        try {
            outputFile.createNewFile();
            FileOutputStream out = new FileOutputStream(outputFile);
            document.writeTo(out);
            document.close();
            out.close();
            Toast.makeText(PdfTabbedActivity.this,
                    "Pdf Saved", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void attachToEmail() {

        String fileName = "/" + invoiceNameDetails.get(0) + ".pdf";
        File fileLocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/UOME", fileName);
        Uri path = Uri.fromFile(fileLocation);

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{clientDetails.get(1)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(PdfTabbedActivity.this, "There are no email clients installed", Toast.LENGTH_LONG).show();
        }

    }

    public static class PlaceholderFragment extends Fragment {
        // Default code I don't want to remove as It causes many things to go wrong
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(activity_pdf_tabbed, container, false);
            return rootView;
        }

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        // Default code I don't want to remove as It causes many things to go wrong
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new PDF_Fragment();
                case 1:
                    return new fragment_save();
            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Preview";
                case 1:
                    return "Save Location";
            }
            return null;
        }
    }


}
