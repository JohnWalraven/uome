package ravensoftware.uome;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;


/**
 * Created by JWalraven on 22/05/2016.
 */
public class XMLFileManager extends ContextWrapper {

    private Invoice newInvoice;
    private FileOutputStream fileos;
    private XmlSerializer xmlSerializer;
    private StringWriter writer;

    public XMLFileManager(Context base, Invoice invoice) {
        super(base);
        this.newInvoice = invoice;
    }

    public void createXMLFile() throws IOException {
        ArrayList<String> invoiceDetails = newInvoice.getInvoiceNameDetails();
        ArrayList<String> clientDetails = newInvoice.getInvoiceClientDetails();
        ArrayList<String> clientAddressDetails = newInvoice.getClientAddressDetails();
        ArrayList<String> itemDetails = newInvoice.getItemDetails();
        ArrayList<String> balancesAndTotals = newInvoice.getBalanceAndTotals();

        try {
            File directory = new File(Environment.getExternalStorageDirectory() + "/UOME");
            File newXmlFile = new File(directory + "/" + invoiceDetails.get(0) + ".xml");
            try {
                if (!directory.exists()) {
                    directory.mkdir();
                }
                if (!newXmlFile.exists()) {
                    newXmlFile.createNewFile();
                }
                System.out.println("Hello World");
            } catch (IOException e) {
                Log.e("IOException", "exception in createNewFile() method");
            }
            try {
                fileos = new FileOutputStream(newXmlFile);
            } catch (FileNotFoundException e) {
                Log.e("FileNotFoundException", "can't create FileOutputStream");
            } finally {
                fileos.close();
            }
            writer = new StringWriter();
            xmlSerializer = Xml.newSerializer();

            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
            xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            //code is indented like this to reflect the output of the XML file
            xmlSerializer.startTag(null, "invoiceList");

            xmlSerializer.startTag(null, "invoice");
            xmlSerializer.startTag(null, "invoiceNumber");
            xmlSerializer.text(invoiceDetails.get(0));
            xmlSerializer.endTag(null, "invoiceNumber");

            xmlSerializer.startTag(null, "businessName");
            xmlSerializer.text(invoiceDetails.get(1));
            xmlSerializer.endTag(null, "businessName");

            xmlSerializer.startTag(null, "date");
            xmlSerializer.text(invoiceDetails.get(2));
            xmlSerializer.endTag(null, "date");

            xmlSerializer.startTag(null, "clientDetails");
            xmlSerializer.startTag(null, "clientName");
            xmlSerializer.text(clientDetails.get(0));
            xmlSerializer.endTag(null, "clientName");
            xmlSerializer.startTag(null, "clientEmail");
            xmlSerializer.text(clientDetails.get(1));
            xmlSerializer.endTag(null, "clientEmail");
            xmlSerializer.startTag(null, "clientMobile");
            xmlSerializer.text(clientDetails.get(2));
            xmlSerializer.endTag(null, "clientMobile");
            xmlSerializer.startTag(null, "clientPhone");
            xmlSerializer.text(clientDetails.get(3));
            xmlSerializer.endTag(null, "clientPhone");
            xmlSerializer.endTag(null, "clientDetails");

            xmlSerializer.startTag(null, "addressDetails");
            xmlSerializer.startTag(null, "addressLine1");
            xmlSerializer.text(clientAddressDetails.get(0));
            xmlSerializer.endTag(null, "addressLine1");
            xmlSerializer.startTag(null, "addressLine2");
            xmlSerializer.text(clientAddressDetails.get(1));
            xmlSerializer.endTag(null, "addressLine2");
            xmlSerializer.startTag(null, "addressLine3");
            xmlSerializer.text(clientAddressDetails.get(2));
            xmlSerializer.endTag(null, "addressLine3");
            xmlSerializer.startTag(null, "addressCity");
            xmlSerializer.text(clientAddressDetails.get(3));
            xmlSerializer.endTag(null, "addressCity");
            xmlSerializer.startTag(null, "addressPostalCode");
            xmlSerializer.text(clientAddressDetails.get(4));
            xmlSerializer.endTag(null, "addressPostalCode");
            xmlSerializer.endTag(null, "addressDetails");

            xmlSerializer.startTag(null, "itemDetails");
            xmlSerializer.startTag(null, "itemDescription");
            xmlSerializer.text(itemDetails.get(0));
            xmlSerializer.endTag(null, "itemDescription");
            xmlSerializer.startTag(null, "itemQuantity");
            xmlSerializer.text(itemDetails.get(1));
            xmlSerializer.endTag(null, "itemQuantity");
            xmlSerializer.startTag(null, "itemCost");
            xmlSerializer.text(itemDetails.get(2));
            xmlSerializer.endTag(null, "itemCost");
            xmlSerializer.startTag(null, "taxable");
            xmlSerializer.text(itemDetails.get(3));
            xmlSerializer.endTag(null, "taxable");
            xmlSerializer.endTag(null, "itemDetails");

            xmlSerializer.startTag(null, "totalBeforeTax");
            xmlSerializer.text(balancesAndTotals.get(0));
            xmlSerializer.endTag(null, "totalBeforeTax");
            xmlSerializer.startTag(null, "taxRate");
            xmlSerializer.text(balancesAndTotals.get(1));
            xmlSerializer.endTag(null, "taxRate");
            xmlSerializer.startTag(null, "total Tax");
            xmlSerializer.text(balancesAndTotals.get(2));
            xmlSerializer.endTag(null, "total Tax");
            xmlSerializer.startTag(null, "balanceDue");
            xmlSerializer.text(balancesAndTotals.get(3));
            xmlSerializer.endTag(null, "balanceDue");

            xmlSerializer.endTag(null, "invoice");
            xmlSerializer.endTag(null, "invoiceList");
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            fileos.write(writer.toString().getBytes());
            fileos.close();
            Toast.makeText(this,
                    "Details Saved!!", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | NullPointerException e) {
            Toast.makeText(XMLFileManager.this, "There appears to be some data missing please try again later", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } finally {
            fileos.close();
        }

    }

}
