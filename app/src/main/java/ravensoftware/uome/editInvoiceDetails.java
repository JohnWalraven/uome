package ravensoftware.uome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

public class editInvoiceDetails extends AppCompatActivity {

    private TextView invoiceName = null;
    private TextView businessName = null;
    private TextView invoiceDate = null;
    private CheckBox dateCheckBox = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Intent intentBackToNewInvoice = new Intent();
        final Bundle extras = new Bundle();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_invoice_details);
        final String currentDateFormat = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());

        Button saveDetailsButton = (Button) findViewById(R.id.editInvoiceSaveDetails);
        invoiceName = (TextView) findViewById(R.id.invoiceName);
        businessName = (TextView) findViewById(R.id.businessName);
        invoiceDate = (TextView) findViewById(R.id.invoiceDate);
        dateCheckBox = (CheckBox) findViewById(R.id.dateCheckBox);

        saveDetailsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkIfNull();
                extras.putString("InvoiceName", invoiceName.getText().toString());
                extras.putString("BusinessName", businessName.getText().toString());

                if (dateCheckBox.isChecked()) {
                    invoiceDate.setText(currentDateFormat);
                    extras.putString("InvoiceDate", invoiceDate.getText().toString());
                    intentBackToNewInvoice.putExtras(extras);
                    setResult(1, intentBackToNewInvoice);
                    finish();
                } else {
                    if (!dateCheckBox.isChecked()) {
                        extras.putString("InvoiceDate", invoiceDate.getText().toString());
                        intentBackToNewInvoice.putExtras(extras);
                        setResult(1, intentBackToNewInvoice);
                        finish();
                    }
                }

            }
        });
    }

    private void checkIfNull() {
        if (invoiceName.getText().toString().equals("") == true) {
            invoiceName.setText("null");
        }
        if (businessName.getText().toString().equals("") == true) {
            businessName.setText("null");
        }
    }
}