package ravensoftware.uome;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import static ravensoftware.uome.R.layout.fragment_pdf_layout;

public class fragment_save extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private Button savePDF;
    private Button emailButton;
    private View rootView;

    private OnFragmentInteractionListener mListener;

    public fragment_save() {
        // Required empty public constructor
    }

    // Default code I don't want to remove as It causes many things to go wrong
    public static fragment_save newInstance(String param1, String param2) {
        fragment_save fragment = new fragment_save();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        savePDF = (Button) getView().findViewById(R.id.savePdf);
        emailButton = (Button) getView().findViewById(R.id.emailButton);
        savePDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PdfTabbedActivity) getActivity()).createPdf();
            }
        });
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PdfTabbedActivity) getActivity()).createPdf();
                ((PdfTabbedActivity) getActivity()).attachToEmail();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(fragment_pdf_layout, container, false);
        return inflater.inflate(R.layout.fragment_save, container, false);
    }

    // Default code I don't want to remove as It causes many things to go wrong
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }


}
